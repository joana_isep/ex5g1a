package com.mycompany.arvore_natal;
import java.util.Scanner;

public class Arvore_Natal {
    public static void main(String[] args) {
        Scanner ler = new Scanner (System.in);
        int X;
        System.out.println("Qual o tamanho da arvore de natal?");
        X = ler.nextInt();
        for (int i=0;i<X;i++){
            for (int j=0; j<X;j++){
                if (i<j)
                    System.out.print("%");
                else
                    System.out.printf(" ");
            }            
            for (int j=X; j>0;j--){
                if (i<j)
                    System.out.print("%");
                else
                    System.out.printf(" ");
            }

            
            System.out.println("");    
        }
    }
    
}
